import asyncio
import subprocess
import telegram
import logging

# Konfigurasi bot Telegram
bot_token = 'TOKEN_BOT'
chat_id = 'CHAT_ID_BOT'

# Konfigurasi logging
logging.basicConfig(filename='/path/file/directory/telegram-notif/log/server_monitoring.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# Daftar server yang akan dimonitor
servers = {
    'Server 1': '192.168.1.11',
    'Server 2': '192.168.1.12',
    'Server 3': '192.168.1.13'
}

# Fungsi untuk mengirim pesan ke bot Telegram
async def send_telegram_message(message):
    bot = telegram.Bot(token=bot_token)
    await bot.send_message(chat_id=chat_id, text=message)

# Fungsi untuk memeriksa apakah server mati atau tidak
async def check_server(server_name, server_ip):
    try:
        # Ping server
        output = subprocess.check_output(['ping', '-c', '1', server_ip])

        # Cek hasil ping
        if '1 received' in output.decode('utf-8'):
            logging.info(f'{server_name} is up')
        else:
            # Kirim laporan ke Telegram jika server mati
            message = f'Server {server_name} ({server_ip}) mati!'
            await send_telegram_message(message)
            logging.warning(message)
    except:
        # Kirim laporan ke Telegram jika server mati
        message = f'Server {server_name} ({server_ip}) mati!'
        await send_telegram_message(message)
        logging.warning(message)

async def main():
    # Jalankan pemeriksaan server secara bersamaan
    await asyncio.gather(*[check_server(name, ip) for name, ip in servers.items()])

if __name__ == '__main__':
    asyncio.run(main())