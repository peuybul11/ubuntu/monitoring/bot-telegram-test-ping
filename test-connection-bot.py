import telegram
import asyncio

# Masukkan token bot Telegram Anda di sini
bot_token = 'TOKEN_BOT'

# Masukkan ID obrolan Telegram yang ingin Anda gunakan untuk pemberitahuan
chat_id = 'CHAT_ID_BOT'

# Fungsi untuk mengirim pesan ke bot Telegram
async def send_telegram_message(message):
    bot = telegram.Bot(token=bot_token)
    await bot.send_message(chat_id=chat_id, text=message)

# Kirim pesan sederhana ke bot Telegram
async def main():
    await send_telegram_message('Testing !!!')
    print('Pesan telah dikirim ke bot Telegram.')

asyncio.run(main())