# Bot Telegram Ping Notifikasi

#### Untuk mendapatkan ID, pertama-tama, poskan pesan apa pun ke Saluran tersebut. Kemudian gunakan kerangka tautan ini untuk mendapatkan ID Saluran:
https://api.telegram.org/botTOKEN_BOT/getUpdates

#### Running Python Script
* sh first-install.sh
* pyhton3 test-connection-bot.py
* python3 server_monitor.py

#### Cronjob 
- (* * * * *) cd /path/file/directory/bot-telegram-test-ping && python3 server-monitoring.py >> /path/file/directory/bot-telegram-test-ping/log/logfile.log 2>&1
